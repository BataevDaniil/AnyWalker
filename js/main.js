var array =
[
	// [0,1,2,3,4,5,6,7,8,9,10,11,0,1,2,3,4,5,6,7,8,9,10,11],
	// [0,1,2,1,4,1,6,1,8,1,10,1,0,1,2,1,4,1,6,1,8,1,10,1],
	// [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1],
	// [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,1,1]
];

var nowPosServe = []
var cellInRow = 24;
var tableDate = document.querySelector("#table-date tbody");
var defaultInputSpeed = "10";
var maxAngle = 650;
var maxSpeed = 1024;

var checkMoveRobIdInterval;
var indexMoveRobo;

//=============================================================================

;(function main()
{
	createTable();
	fillTable();
	createInput();
	readFile();
	downloadFile();
	// console.log( strInTopic(1) );
	// conectRos();
}());
//==============================================================================

setInterval(function()
{

}, 100);

function downloadFile()
{

	document.getElementById("but-download").onclick = function()
	{
		var str = "";
		for (var i = 0, countI = array.length; i < countI; i++)
		{
			for (var j = 0, countJ = array[i].length; j < countJ; j++)
			{
				str += array[i][j] + ",";
			}
			str = str.slice(0,-1) + "\n";
		}
		str = str.slice(0,-1);
		this.href = URL.createObjectURL(new Blob([str], {type: 'text/csv'}));
		this.download = 'table.csv';
	}
};
//==============================================================================

function readFile()
{
	file = document.querySelector("#but-save-table");

	file.onchange = function()
	{
		var file = this.files[0];
		var readFile = new FileReader();
		readFile.readAsText(file);
		readFile.onload = function()
		{
			fileStr = this.result;

			deleteTable();
			array.push([]);
			for (var index=0, i=0, j=0, str=""; index < fileStr.length; index++)
			{
				while (fileStr[index] !== "," && fileStr[index] !== "\n" && index < fileStr.length)
				{
					str += fileStr[index];
					index++;
				}
				array[i][j] = +str;
				str = "";
				j++;
				if (fileStr[index] === "\n") {i++; array.push([]); j=0;}
			}
			createTable();
			fillTable();
		}
	}
};
//==============================================================================

function rmRowTable(i)
{
	if (i < tableDate.rows.length && i >= 0)
	{
		array.splice(i, 1);
		tableDate.removeChild( tableDate.rows[i] );

		if (i > 1)
		{
			tableDate.rows[tableDate.rows.length-2].onmouseover = checkColorRow;
			tableDate.rows[tableDate.rows.length-2].onmouseout = resetColorRow;
			tableDate.rows[tableDate.rows.length-2].onclick = copyInInput;
		}
	}
};
//==============================================================================

function fillRowTable(i)
{
	for (var j = 0; j < array[i].length; j++)
	{
		if (i%2 === 0 )
			if (j % 2 === 1 && array[i][j] === 0)
				tableDate.rows[i].cells[j+1].innerHTML = "";
			else tableDate.rows[i].cells[j+1].innerHTML = array[i][j];
		else
		{
			if (j % 2 === 1)
			{
				if (array[i][j])
					tableDate.rows[i].cells[j].innerHTML = '<img src="img/verdana.png" alt="">';
				else tableDate.rows[i].cells[j].innerHTML = "";
			}
			else tableDate.rows[i].cells[j].innerHTML = array[i][j];
		}
	}
};

//==============================================================================

function fillTable()
{
	for (var i = 0; i < array.length; i++)
		fillRowTable(i);
};
//==============================================================================

function deleteTable()
{
	for (var i = array.length-1; i >= 0; i--)
		rmRowTable(i);
};
//==============================================================================

function addRowTable(numLine)
{
	var trMain = document.createElement("tr");

	if (numLine%2 === 0)
	{
		var tdNumLine = document.createElement("td");
		tdNumLine.setAttribute("rowspan", "2");
		tdNumLine.innerHTML = (numLine/2 + 1) + "";
		trMain.appendChild(tdNumLine);
	}

	for (var j = 0; j < cellInRow; j++)
	{
		var td = document.createElement("td");
		trMain.appendChild(td);
	}

	var len = tableDate.rows.length;

	if (len > 1)
	{
		tableDate.rows[len - 2].onclick = null;
		tableDate.rows[len - 2].onmouseover = null;
		tableDate.rows[len - 2].onmouseout = null;
	}

	trMain.onmouseover = checkColorRow;
	trMain.onmouseout = resetColorRow;

	trMain.onclick = copyInInput;

	if (tableDate.lastElementChild)
	{
		var posit = tableDate.lastElementChild.getBoundingClientRect();
		var but = document.querySelector("#but-del-row");
		but.style.top = (posit.top + pageYOffset) + "px";
		but.style.left = (posit.right + pageXOffset + 20) + "px";
	}

	return trMain;
};
//==============================================================================

function createTable()
{
	var row = array.length;

	for (var i = 0; i < row; i++)
	{
		tableDate.appendChild(addRowTable(i));
	}
};
//==============================================================================

function createInput()
{
	//фильтруем ввод только цифр полям ввода в таблицу
	var temp = document.querySelectorAll('#write-date div input[type="text"]');
	for (var i = 0; i < temp.length; i++)
	{
		temp[i].addEventListener("input", function()
		{
			if ( this.value.length === 1 && this.value === "-" )
				;
			else if ( isNaN( this.value ) )
				this.value = this.value.slice(0, -1);
		});
	}

	//создаем массив
	var temp1 = document.querySelectorAll("#L-top-field input");
	var temp2 = document.querySelectorAll("#R-top-field input");
	var input = joinPsevdaArr(temp1, temp2);
	for (var i = 1; i < input.length; i+=2)
	{
		input[i].disabled = true;
		input[i].addEventListener("focus", function()
		{
			this.setSelectionRange(0, this.value.length);
		});

		input[i].addEventListener("input", function()
		{
			var but = document.querySelector("#but-add-row");
			var sum = +this.value;
			if (0 <= sum && sum < maxSpeed)
			{
				this.style.background = "";
				but.disabled = false;
			}
			else
			{
				this.style.background = "#f00";
				but.disabled = true;
			}
		});
	}

	for (var i = 0; i < input.length; i+=2)
	{
		input[i].addEventListener("input", function()
		{
			var i = numPosFromParent(this);
			var j = numPosFromParent(this.parentElement);

			var input = document.querySelector("#table-input");
			var but = document.querySelector("#but-add-row");

			var sum = +this.value + array[array.length-1][i];
			if (Math.abs( sum ) < maxAngle)
			{
				input.children[j+1].children[i].value = sum;
				input.children[j].children[i].style.background = "";
				but.disabled = false;
			}
			else
			{
				input.children[j].children[i].style.background = "#f00";
				but.disabled = true;
			}
		});
	}

	var temp1 = document.querySelectorAll("#L-bot-field input");
	var temp2 = document.querySelectorAll("#R-bot-field input");
	var input = joinPsevdaArr(temp1, temp2);
	for (var i = 1; i < input.length; i+=2)
	{
		input[i].checked = 1;
		input[i].onchange = function()
		{

			var i = numPosFromParent(this);
			var j = numPosFromParent(this.parentElement);

			var input = document.querySelector("#table-input");
			if (input.children[j-1].children[i].disabled)
			{
				input.children[j-1].children[i].disabled = false;
				input.children[j-1].children[i].value = defaultInputSpeed;
				input.children[j-1].children[i].focus();
			}
			else
			{
				var but = document.querySelector("#but-add-row");
				but.disabled = false;
				input.children[j-1].children[i].disabled = true;
				input.children[j-1].children[i].style.background = "";
				input.children[j-1].children[i].value = "";
			}
		}
	}

	for (var i = 0; i < input.length; i+=2)
	{
		input[i].addEventListener("input", function()
		{
			var i = numPosFromParent(this);
			var j = numPosFromParent(this.parentElement);

			var input = document.querySelector("#table-input");
			var but = document.querySelector("#but-add-row");

			var sum = +this.value - array[array.length-1][i];
			if (Math.abs( +this.value ) < maxAngle)
			{
				input.children[j-1].children[i].value = sum;
				input.children[j].children[i].style.background = "";
				but.disabled = false;
			}
			else
			{
				input.children[j].children[i].style.background = "#f00";
				but.disabled = true;
			}
		});
	}
	document.querySelector("#L-top-field").children[0].autofocus = true;

	document.getElementById("but-add-row").onclick = function()
	{
		tableDate.appendChild( addRowTable( array.length ) );
		tableDate.appendChild( addRowTable( array.length+1 ) );

		var temp1 = document.querySelectorAll("#L-top-field input");
		var temp2 = document.querySelectorAll("#R-top-field input");
		var input = joinPsevdaArr(temp1, temp2);
		array[array.length] = [];
		for (var i = 0, j = array.length-1; i < input.length; i++)
			array[j][i] = +input[i].value;

		var temp1 = document.querySelectorAll("#L-bot-field input");
		var temp2 = document.querySelectorAll("#R-bot-field input");
		var input = joinPsevdaArr(temp1, temp2);
		array[array.length] = [];
		for (var i = 0, j = array.length-1; i < input.length; i+=2)
		{
			array[j][i] = +input[i].value;
			array[j][i+1] = +input[i+1].checked;
		}

		var input = document.querySelectorAll('#table-input input[type="text"]');
		for (var i = 0; i < input.length; i++)
			input[i].value = "";

		var input = document.querySelectorAll('#table-input input[type="checkbox"]');
		for (var i = 0; i < input.length; i++)
			input[i].checked = true;

		var temp1 = document.querySelectorAll("#L-top-field input:nth-child(2n)");
		var temp2 = document.querySelectorAll("#R-top-field input:nth-child(2n)");
		var input = joinPsevdaArr(temp1,temp2);
		for (var i = 0; i < input.length; i++)
			input[i].disabled = true;

		for (var i = 0, count = array[array.length-1].length; i < count; i++)
		{
			var num0 = array[array.length-2][i];
			var num1 = array[array.length-1][i];
			var lastNumArr = array[array.length-3][i];

			if (i % 2 === 0)
			{
				if (num0 && num1)
					;
				else
				{
					num0 = 0;
					num1 = lastNumArr;
				}
			}
			else
			{
				if (num1)
					num0 = 0;
				else
				{
					if(num0)
						;
					else num0 = defaultInputSpeed;
				}
			}

			array[array.length-2][i] = +num0;
			array[array.length-1][i] = +num1;
		}
		fillRowTable(array.length-1);
		fillRowTable(array.length-2);
	}

	document.getElementById("but-del-row").onclick = function()
	{
		if (tableDate.rows.length > 1)
		{
			rmRowTable(tableDate.rows.length-1);
			rmRowTable(tableDate.rows.length-1);

			if (tableDate.lastElementChild)
			{
				var posit = tableDate.lastElementChild.getBoundingClientRect();
				this.style.top = (posit.top + pageYOffset - 25) + "px";
				this.style.left = (posit.right + pageXOffset + 20) + "px";
			}
		}
	}

	document.getElementById("move-robot").onclick = function()
	{
		checkMoveRobIdInterval = setInterval(function()
		{
			var Yn = true;
			for (var i = 0; i < nowPosServe.length; i++)
				if (nowPosServe[i] !== array[indexMoveRobo][i])
				{
					Yn = false;
					break;
				}

			if (Yn)
				if (indexMoveRobo == array.length - 1)
				{
					indexMoveRobo = 0;
					clearInterval( checkMoveRobIdInterval );
				}
				else
					indexMoveRobo++;
		}, 20);
	}
};
//==============================================================================

function copyInInput()
{
	var str = "#L-top-field input:nth-child(2n+1), #R-top-field input:nth-child(2n+1)"
	var input = document.querySelectorAll( str );
	for (var i = 0; i < input.length; i++)
		input[i].value = 0;

	var str = "#L-bot-field input:nth-child(2n+1), #R-bot-field input:nth-child(2n+1)"
	var input = document.querySelectorAll( str );
	for (var i = 0; i < input.length; i++ )
		input[i].value = array[array.length - 1][i*2];

	var str = "#L-bot-field input:nth-child(2n), #R-bot-field input:nth-child(2n)"
	var inputCheckbox = document.querySelectorAll( str );
	for (var i = 0; i < inputCheckbox.length; i++)
		inputCheckbox[i].checked = array[array.length - 1][i*2 + 1];

	var str = "#L-top-field input:nth-child(2n), #R-top-field input:nth-child(2n)"
	var input = document.querySelectorAll( str );
	for (var i = 0; i < input.length; i++)
	{
		if ( inputCheckbox[i].checked )
		{
			input[i].disabled = true;
			input[i].value = "";
		}
		else
		{
			input[i].disabled = false;
			input[i].value = array[array.length - 2][i*2 + 1];
		}
	}
};
//==============================================================================

function checkColorRow()
{
	this.style.background = "#E324D6";
	if (this.nextElementSibling)
		this.nextElementSibling.style.background = "#E324D6";
	else this.previousElementSibling.style.background = "#E324D6";
};
//==============================================================================

function resetColorRow()
{
	this.style.background = "";
	if (this.nextElementSibling)
		this.nextElementSibling.style.background = "";
	else this.previousElementSibling.style.background = "";
};
//==============================================================================

function conectRos()
{
	//Connected ROS
	var ros = new ROSLIB.Ros({
		url : 'ws://'+document.location.hostname+':9090'
	});

	ros.on('connection', function() {
		console.log('Connected to websocket server.');
	});

	ros.on('error', function(error) {
		console.log('Error connecting to websocket server: ', error);
	});

	ros.on('close', function() {
		console.log('Connection to websocket server closed.');
	});

	var topicCONTROL = new ROSLIB.Topic({
		ros : ros,
		name : '/aw_driver/js_py',
		messageType : 'std_msgs/String'
	});

	var topicSTATUS = new ROSLIB.Topic({
		ros : ros,
		name : '/aw_driver/status/robot_status',
		messageType : 'anywalker_msgs/RobotStatus'
	});

	angleStatus.subscribe(function(msg)
	{
		var i = 0;
		for (; i < 6; i++)
			nowPosServe[i] = msg.left_leg[i];

		for (; i < 12; i++)
			nowPosServe[i] = msg.left_leg[i-6];
	});


};
//==============================================================================

function strInTopic(index)
{
	//s50,ml1-10,s20,mr650
	console.log(index);
	index *= 2;
	index++;
	console.log(index);
	var str = '';

	var i = 1, count = cellInRow/2
	for (; i < count; i += 2)
	{
		if (array[index][i])
			str += 'o' + ','; //автоспид
		else
			str += 's' + array[index-1][i] + ','; //скорост сервы

		str += 'ml' + ((i+1)/2);
		str += array[index][i-1] + ',';//поворот сервы
	}

	for (; i < count*2; i+=2)
	{
		if (array[index][i])
			str += 'o' + ','; //автоспид
		else
			str += 's' + array[index-1][i] + ','; //скорост сервы

		str += 'mr' + ((i+1)/2);
		str += array[index][i-1] + ',';//поворот сервы
	}

	return str;
};
//==============================================================================

function joinPsevdaArr()
{
	var arr = [];
	for (var i = 0, count = 0; i < arguments.length; i++)
		for (var j = 0; j < arguments[i].length; j++, count++)
			arr[count] = arguments[i][j];
	return arr;
};
//==============================================================================

function numPosFromParent(elem)
{
	var  i = 0;
	while ( (elem=elem.previousElementSibling) != null ) i++;
	return i;
};
//==============================================================================